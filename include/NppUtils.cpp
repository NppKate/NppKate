/*
Copyright (c) 2017-2018, Schadin Alexey (schadin@gmail.com)
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions
and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
and the following disclaimer in the documentation and/or other materials provided with
the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse
or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <filesystem>
#include "NppUtils.h"

namespace fs = std::experimental::filesystem;

NppUtils::NppUtils(HANDLE hInstance, const NppData& npp_data, TCHAR *plugin_name) : hInstance_{ hInstance }, npp_data_{ npp_data }, plugin_name_{ plugin_name }
{
}


NppUtils::~NppUtils()
{
}

std::wstring NppUtils::plugin_dir()
{
    if (plugin_dir_.empty()) {
        TCHAR buffer[MAX_PATH + 1] = { 0 };
        GetModuleFileName((HMODULE)hInstance_, buffer, MAX_PATH);
        auto result = std::wstring(buffer);
        auto parent_path = fs::path(result).parent_path().make_preferred();
        result = parent_path.append(plugin_name_).wstring();
        if (!fs::exists(result))
            fs::create_directories(result);
        plugin_dir_.swap(result);
    }
    return plugin_dir_;
}

std::wstring NppUtils::log()
{
    if (log_file_.empty()) {
        auto result = fs::path(config_dir()).append(plugin_name_ + _TEXT(".log"));
        log_file_ = result.wstring();
    }
    return log_file_;
}

long NppUtils::current_line() const
{
    return (long) SendMessage(npp_data_._nppHandle, NPPM_GETCURRENTLINE, 0, 0) + 1;
}

std::wstring NppUtils::current_file_path() const
{
    TCHAR buffer[MAX_PATH + 1] = { 0 };
    SendMessage(npp_data_._nppHandle, NPPM_GETFULLCURRENTPATH, MAX_PATH, (LPARAM)&buffer);
    return std::wstring(buffer);
}

HWND NppUtils::view_handle() const
{
    int curScintilla = 0;
    SendMessage(npp_data_._nppHandle, NPPM_GETCURRENTSCINTILLA, 0, (LPARAM)&curScintilla);
    return curScintilla == 0 ? npp_data_._scintillaMainHandle : npp_data_._scintillaSecondHandle;
}

HWND NppUtils::npp_handle() const
{
    return npp_data_._nppHandle;
}

std::wstring NppUtils::config_dir()
{
    if (config_dir_.empty()) {
        TCHAR buffer[MAX_PATH + 1] = { 0 };
        SendMessage(npp_data_._nppHandle, NPPM_GETPLUGINSCONFIGDIR, MAX_PATH, (LPARAM)&buffer);
        auto result = fs::path(buffer);
        result.append(plugin_name_);
        if (!fs::exists(result.wstring()))
            fs::create_directories(result.wstring());
        config_dir_ = result.wstring();
    }
    return config_dir_;

}

std::wstring NppUtils::config_file()
{
    auto config_path = fs::path(config_dir());
    config_path.append(plugin_name_ + _TEXT(".ini"));
    return config_path.wstring();
}
