/*
Copyright (c) 2017-2018, Schadin Alexey (schadin@gmail.com)
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions
and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
and the following disclaimer in the documentation and/or other materials provided with
the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse
or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#include <string>
#include <map>
#include <NppUtils.h>
#include "Config.h"

enum CloseOnEnd : int
{
    CloseManual = 0,
    AutoClose = 1,
    AutoCloseIfNoErr = 2
};

class Tortoise
{
public:
    Tortoise(NppUtils &npp_utils, Config &config);
    ~Tortoise();

    void pull();
    void fetch();
    void push();
    void add();
    void blame();
    void blame_line();
    void commit();
    void repostatus();
    void rebase();
    void log();
    void log_file();
    void stash_save();
    void stash_apply();
    void stash_pop();
    void rename();
    void remove();
    void checkout();
    void merge();
    void settings();

    void select_bin_folder();
private:
    NppUtils &npp_utils_;
    Config &config_;
    std::wstring exe_path_;

    void tortoise_command(const std::wstring, const std::wstring path, std::map<std::wstring, std::wstring> params, CloseOnEnd close_end);
    std::wstring git_path();
};

