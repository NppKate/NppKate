/*
Copyright (c) 2017-2018, Schadin Alexey (schadin@gmail.com)
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions
and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
and the following disclaimer in the documentation and/or other materials provided with
the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse
or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <Windows.h>
#include <vector>
#include <PluginInterface.h>
#include <NppKate.h>
#include <NppUtils.h>
#include "Tortoise.h"

using namespace std;

HANDLE hInstance;
vector<FuncItem> commands_;
NppUtils *nuNppUtils = nullptr;
int count_command_;
Config *config;
Tortoise *tortoise = nullptr;

BOOL APIENTRY DllMain(HANDLE hModule, DWORD  reasonForCall, LPVOID /*lpReserved*/)
{
    switch (reasonForCall)
    {
    case DLL_PROCESS_ATTACH:
        hInstance = hModule;
        break;

    case DLL_PROCESS_DETACH:
        if (nuNppUtils) {
            delete nuNppUtils;
        }
        commands_.resize(0);
        if (tortoise) {
            delete tortoise;
        }
        if (config) {
            delete config;
        }
        break;

    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
        break;
    }

    return TRUE;
}

size_t registerCommand(const TCHAR *name, PFUNCPLUGINCMD handler, bool is_checked = false, ShortcutKey *shortcut = nullptr) {
    size_t index = commands_.size();
    FuncItem *item;
    commands_.resize(index + 1);
    item = &commands_[index];

    ZeroMemory(item->_itemName, sizeof(TCHAR) * nbChar);
    lstrcpyn(item->_itemName, name, nbChar - 1);
    item->_pFunc = handler;
    item->_init2Check = is_checked;
    item->_pShKey = shortcut;
    return index;
};

extern "C" {

    __declspec(dllexport) void setInfo(NppData npp_date)
    {
        //
        nuNppUtils = new NppUtils(hInstance, npp_date, KATE_TG);
        config = new Config(nuNppUtils->config_file());
        tortoise = new Tortoise(*nuNppUtils, *config);
        // commands
        if (config->tortoise_proc_path().empty()) {
            registerCommand(_TEXT("Select TortoiseGit folder"), []() { tortoise->select_bin_folder(); }, false, nullptr);
            registerCommand(TEXT("-"), nullptr);
        }
        registerCommand(_TEXT("Pull..."), []() { tortoise->pull(); }, false, nullptr);
        registerCommand(_TEXT("Fetch..."), []() { tortoise->fetch(); }, false, nullptr);
        registerCommand(_TEXT("Push..."), []() { tortoise->push(); }, false, nullptr);
        registerCommand(TEXT("-"), nullptr);
        registerCommand(_TEXT("Add"), []() { tortoise->add(); }, false, nullptr);
        registerCommand(_TEXT("Blame file"), []() { tortoise->blame(); }, false, nullptr);
        registerCommand(_TEXT("Blame current line"), []() { tortoise->blame_line(); }, false, nullptr);
        registerCommand(_TEXT("Commit..."), []() { tortoise->commit(); }, false, nullptr);
        registerCommand(_TEXT("Check for modifications"), []() { tortoise->repostatus(); }, false, nullptr);
        registerCommand(_TEXT("Rebase..."), []() { tortoise->rebase(); }, false, nullptr);
        registerCommand(_TEXT("Log repository"), []() { tortoise->log(); }, false, nullptr);
        registerCommand(_TEXT("Log file"), []() { tortoise->log_file(); }, false, nullptr);
        registerCommand(TEXT("-"), nullptr);
        registerCommand(_TEXT("Stash save"), []() { tortoise->stash_save(); }, false, nullptr);
        registerCommand(_TEXT("Stash apply"), []() { tortoise->stash_apply(); }, false, nullptr);
        registerCommand(_TEXT("Stash pop"), []() { tortoise->stash_pop(); }, false, nullptr);
        registerCommand(TEXT("-"), nullptr);
        registerCommand(_TEXT("Rename..."), []() { tortoise->rename(); }, false, nullptr);
        registerCommand(_TEXT("Delete"), []() { tortoise->remove(); }, false, nullptr);
        registerCommand(_TEXT("Switch/Checkout..."), []() { tortoise->checkout(); }, false, nullptr);
        registerCommand(_TEXT("Merge"), []() { tortoise->merge(); }, false, nullptr);
        registerCommand(TEXT("-"), nullptr);
        registerCommand(_TEXT("Settings"), []() { tortoise->settings(); }, false, nullptr);
        // end commands
        count_command_ = (int)commands_.size();
    }

    __declspec(dllexport) const TCHAR * getName()
    {
        return KATE_TG;
    }

    __declspec(dllexport) FuncItem * getFuncsArray(int *count_command)
    {
        *count_command = count_command_;
        return &(commands_[0]);
    }

    __declspec(dllexport) void beNotified(SCNotification *)
    {
        //
    }

    __declspec(dllexport) LRESULT messageProc(UINT, WPARAM, LPARAM)
    {
        return 0;
    }

    #ifdef UNICODE
    __declspec(dllexport) BOOL isUnicode()
    {
        return TRUE;
    }
    #endif //UNICODE
}