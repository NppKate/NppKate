/*
Copyright (c) 2017-2018, Schadin Alexey (schadin@gmail.com)
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions
and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions
and the following disclaimer in the documentation and/or other materials provided with
the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse
or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <Windows.h>
#include <vector>
#include <PluginInterface.h>
#include <NppKate.h>
#include <NppUtils.h>
#include "GitExtensions.h"

using namespace std;

HANDLE hInstance;
vector<FuncItem> commands_;
NppUtils *nuNppUtils = nullptr;
int count_command_;
Config *config;
GitExtensions *git_ext = nullptr;

BOOL APIENTRY DllMain(HANDLE hModule, DWORD  reasonForCall, LPVOID /*lpReserved*/)
{
    switch (reasonForCall)
    {
    case DLL_PROCESS_ATTACH:
        hInstance = hModule;
        break;

    case DLL_PROCESS_DETACH:
        if (nuNppUtils) {
            delete nuNppUtils;
        }
        commands_.resize(0);
        if (git_ext) {
            delete git_ext;
        }
        if (config) {
            delete config;
        }
        break;

    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
        break;
    }

    return TRUE;
}

size_t registerCommand(const TCHAR *name, PFUNCPLUGINCMD handler, bool is_checked = false, ShortcutKey *shortcut = nullptr) {
    size_t index = commands_.size();
    FuncItem *item;
    commands_.resize(index + 1);
    item = &commands_[index];

    ZeroMemory(item->_itemName, sizeof(TCHAR) * nbChar);
    lstrcpyn(item->_itemName, name, nbChar - 1);
    item->_pFunc = handler;
    item->_init2Check = is_checked;
    item->_pShKey = shortcut;
    return index;
};

extern "C" {

    __declspec(dllexport) void setInfo(NppData npp_date)
    {
        //
        nuNppUtils = new NppUtils(hInstance, npp_date, KATE_GEXT);
        config = new Config(nuNppUtils->config_file());
        git_ext = new GitExtensions(*nuNppUtils, *config);
        // commands
        if (config->git_ext_path().empty()) {
            registerCommand(_TEXT("Select GitExtensions folder"), []() { git_ext->select_bin_folder(); }, false, nullptr);
            registerCommand(TEXT("-"), nullptr);
        }
        registerCommand(_TEXT("Browse"), []() { git_ext->browse(); }, false, nullptr);
        registerCommand(_TEXT("Commit"), []() { git_ext->commit(); }, false, nullptr);
        registerCommand(_TEXT("Pull"), []() { git_ext->pull(); }, false, nullptr);
        registerCommand(_TEXT("Fetch"), []() { git_ext->fetch(); }, false, nullptr);
        registerCommand(_TEXT("Push"), []() { git_ext->push(); }, false, nullptr);
        registerCommand(TEXT("-"), nullptr);
        registerCommand(_TEXT("Checkout"), []() { git_ext->checkout(); }, false, nullptr);
        registerCommand(_TEXT("Branch"), []() { git_ext->branch(); }, false, nullptr);
        registerCommand(_TEXT("Rebase"), []() { git_ext->rebase(); }, false, nullptr);
        registerCommand(_TEXT("Blame"), []() { git_ext->blame(); }, false, nullptr);
        registerCommand(_TEXT("Stash"), []() { git_ext->stash(); }, false, nullptr);
        registerCommand(_TEXT("Merge"), []() { git_ext->merge(); }, false, nullptr);
        registerCommand(_TEXT("Cherry pick"), []() { git_ext->cherry(); }, false, nullptr);
        registerCommand(TEXT("-"), nullptr);
        registerCommand(_TEXT("Git bash"), []() { git_ext->gitbash(); }, false, nullptr);
        registerCommand(_TEXT("Settings"), []() { git_ext->settings(); }, false, nullptr);
        // end commands
        count_command_ = (int)commands_.size();
    }

    __declspec(dllexport) const TCHAR * getName()
    {
        return KATE_GEXT;
    }

    __declspec(dllexport) FuncItem * getFuncsArray(int *count_command)
    {
        *count_command = count_command_;
        return &(commands_[0]);
    }

    __declspec(dllexport) void beNotified(SCNotification *)
    {
        //
    }

    __declspec(dllexport) LRESULT messageProc(UINT, WPARAM, LPARAM)
    {
        return 0;
    }

    #ifdef UNICODE
    __declspec(dllexport) BOOL isUnicode()
    {
        return TRUE;
    }
    #endif //UNICODE
}